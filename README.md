# Dirclean 
 
##Summary
A drop-in replacement for the TRU64 utility with the same name, written in perl

##Usage

##Examples

##History

##Caveat
This script removes files from your system. Incorrect use can damage
your system. TEST CAREFULLY!

##Recommendations
Under root, use this script with the -chroot option. This restricts all
possible actions of this script to one directory and its subdirectories.
This is a safety measure, to prevent this script from running amok
on the rest of your system with root privileges.
