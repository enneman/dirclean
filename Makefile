PROGRAM=dirclean
PREFIX=${HOME}

gitpush:
	git push -u origin master

install: $(PROGRAM)
	@echo "Installing dirclean..."
	install -m 0755 $(PROGRAM) $(PREFIX)/bin

clean: 
	rm *~
